﻿using NUnit.Framework;
using System;
using NSubstitute;

using static TddKata.Pile;

namespace TddKata
{
	[TestFixture]
	public class PileTest
	{
		private Pile pile;
		private static int anElement = 5;
		private static int otherElement = 42;

		[SetUp]
		public void setUp()
		{
			pile = new Pile ();
		}

		[Test]
		public void newPile_shouldBeEmpty()
		{
			Assert.True (pile.isEmpty ());
		}

		[Test]
		public void isEmpty_withAnElementInThePile_shouldReturnFalse()
		{
			pile.push (anElement);
			Assert.False (pile.isEmpty ());
		}

		[Test]
		public void pop_withOneElement_shouldIt()
		{
			pile.push (anElement);
			var poppedElement = pile.pop ();
			Assert.AreEqual (anElement, poppedElement);
		}

		[Test]
		public void pop_withManyElements_shouldReturnLastPushed()
		{
			pile.push (anElement);
			pile.push (otherElement);

			var poppedElement = pile.pop ();

			Assert.AreEqual (otherElement, poppedElement);
		}

		[Test]
		public void pop_withOneElement_shouldBeEmpty()
		{
			pile.push (anElement);
			pile.pop ();
			Assert.True (pile.isEmpty ());
		}
	}
}
