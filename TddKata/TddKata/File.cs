﻿using System;

namespace TddKata
{
	public class File
	{
		bool vide = true;
		String premier = "";

		public bool estVide()
		{
			return vide;
		}

		public void ajouter(String element)
		{
			vide = false;
			premier = element;
		}

		public String pop()
		{
			return premier;
		}
	}
}

