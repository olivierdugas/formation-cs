﻿using NUnit.Framework;
using System;
using NSubstitute;

namespace TddKata
{
	[TestFixture ()]
	public class GrilleTest
	{
		private Combinateur combinateur = Substitute.For<Combinateur> ();
		private Grille grille;
		private int[] resultat;

		[SetUp]
		public void setUp()
		{
			grille = new Grille (combinateur);
		}

		[Test]
		public void gaucheDevraitConcatener4Lignes ()
		{
			devraitConcatener(new int[] { 1, 2, 3, 4 }, new int[] { 4, 3, 2, 1 });
			devraitConcatener(new int[] { 5, 6, 7, 8 }, new int[] { 8, 7, 6, 5 });
			devraitConcatener(new int[] { 9, 10,11,12}, new int[] { 12,11,10, 9});
			devraitConcatener(new int[] { 13,14,15,16}, new int[] { 16,15,14,13});

			int[] resultat = grille.gauche(new int[]
				{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16});
			
			int[] attendu = new int[]
			    {16, 15, 14, 13, 12, 11, 10, 9,	8, 7, 6, 5,	4, 3, 2, 1};
			Assert.AreEqual (attendu, resultat);
		}

		[Test]
		public void uneSeuleCaseVide_ApresConcatenation_devraitEtreRemplaceePar2()
		{
			int[] grilleDepart = apresConcatenation (new int[] 
			{
				1, 1, 1, 1,
				1, 0, 1, 1,
				1, 1, 1, 1,
				1, 1, 1, 1
			});

			resultat = grille.gauche (grilleDepart);

			comparerGrilleAttendue(new int[] 
			{
				1, 1, 1, 1,
				1, 2, 1, 1,
				1, 1, 1, 1,
				1, 1, 1, 1
			});
		}

		private void devraitConcatener(int[] avant, int[] apres)
		{
			combinateur.concatener (avant).Returns (apres);
		}

		private int[] apresConcatenation(int[] apres)
		{
			int[] depart = new int[] { 0,0,0,0, 1,1,1,1, 2,2,2,2, 3,3,3,3};
			devraitConcatener (new int[] { 0, 0, 0, 0 }, new int[] { apres [0], apres [1], apres [2], apres [3] });
			devraitConcatener (new int[] { 1, 1, 1, 1 }, new int[] { apres [4], apres [5], apres [6], apres [7] });
			devraitConcatener (new int[] { 2, 2, 2, 2 }, new int[] { apres [8], apres [9], apres [10], apres [11] });
			devraitConcatener (new int[] { 3, 3, 3, 3 }, new int[] { apres [12], apres [13], apres [14], apres [15] });
		}
	}
}

