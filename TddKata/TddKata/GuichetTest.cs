﻿using NUnit.Framework;
using System;
using NSubstitute;

namespace TddKata
{
	[TestFixture ()]
	public class GuichetTest
	{
		private int unMontant = 42;
		[Test ()]
		public void devraitDebiterCompte_lorsquAssezDargent ()
		{
			Compte compte = Substitute.For<Compte> ();
			Guichet guichet = new Guichet (compte);
			compte.assezDArgent (unMontant).Returns (true);

			guichet.payer (unMontant);

			compte.Received ().debiter (unMontant);
		}

		[Test]
		public void devraitLancerException_siPauvre()
		{
			Compte compte = Substitute.For<Compte> ();
			Guichet guichet = new Guichet (compte);
			compte.assezDArgent (unMontant).Returns (false);

			Assert.Throws<PasAssezDArgentException>(() => guichet.payer (unMontant));
		}
	}
}

