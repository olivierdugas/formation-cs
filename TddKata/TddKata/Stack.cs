﻿using System;

namespace TddKata
{
	public class Stack
	{
		private bool empty = true;
		private int lastPushed = 0;
		public bool isEmpty()
		{
			return empty;
		}

		public void push(int element)
		{
			empty = false;
			lastPushed = element;
		}

		public int pop()
		{
			empty = true;
			return lastPushed;
		}
	}
}

