﻿using System;

namespace TddKata
{
	public class Pile
	{
		private bool empty;
		private int element;

		public Pile ()
		{
			empty = true;
		}

		public bool isEmpty()
		{
			return empty;
		}

		public void push(int element)
		{
			empty = false;
			this.element = element;
		}

		public int pop()
		{
			empty = true;
			return element;
		}
	}
}
