﻿using NUnit.Framework;
using System;

namespace TddKata
{
	[TestFixture ()]
	public class StackTest
	{
		private Stack stack;
		private int anyElement = 42;
		private int lastElement = 665;
	
		[SetUp]
		public void setUp()
		{
			stack = new Stack ();
		}
		[Test ()]
		public void newStack_shouldBeEmpty ()
		{
			Assert.IsTrue(stack.isEmpty ());
		}

		[Test]
		public void stack_withAnElement_shouldNotBeEmpty()
		{
			stack.push (anyElement);
			Assert.False (stack.isEmpty ());
		}

		[Test]
		public void popping_anElement_shouldReturnLastPushed()
		{
			stack.push (anyElement);
			int received = stack.pop ();
			Assert.AreEqual (anyElement, received);
		}

		[Test]
		public void popping_withOneElement_shouldReturnToEmpty()
		{
			stack.push (anyElement);
			stack.pop ();
			Assert.IsTrue (stack.isEmpty ());
		}

		[Test]
		public void popping_withTwoElements_shouldReturnLastPushed()
		{
			stack.push (anyElement);
			stack.push (lastElement);

			int received = stack.pop ();

			Assert.AreEqual (lastElement, received);
		}


	}
}

