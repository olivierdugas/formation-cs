﻿using NUnit.Framework;
using System;

namespace TddKata
{
	[TestFixture ()]
	public class FileTest
	{
		private File file;
		String premier = "element";

		[SetUp]
		public void setUp()
		{
			file = new File ();
		}

		[Test ()]
		public void nouvelleFile_estVide ()
		{
			Assert.IsTrue(file.estVide ());
		}

		[Test]
		public void ajouterElement_rendLaFileNonVide()
		{
			file.ajouter (premier);
			Assert.IsFalse (file.estVide ());
		}

		[Test]
		public void pop_retourneLePremierElement()
		{
			file.ajouter (premier);
			String recu = file.pop ();
			Assert.AreEqual (premier, recu);
		}
	}
}

