﻿using System;

namespace TddKata
{
	public class PasAssezDArgentException : Exception
	{
	}

	public class Compte
	{
		public virtual bool assezDArgent(int montant)
		{
			//appel EVIL
			return true;
		}
		public virtual void debiter(int montant)
		{
			//appel à EVIL 2 fois!!!
		}
	}

	public class Guichet
	{
		private Compte compte;
		public Guichet(Compte compte)
		{
			this.compte = compte;
		}

		public void payer(int montant)
		{
			if (compte.assezDArgent (montant)) {
				compte.debiter (montant);
			} else {
				throw new PasAssezDArgentException ();
			}
					
		}
	}
}

